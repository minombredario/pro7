<?php
require ("funciones.php");




$modelo = getModelo();

        //PROFESORES 

            if(isset($_GET['crearprof'])){
               
                $id = recoge('id');
                $nombre = recoge('nombre');

                $profesor = new Profesor($id, $nombre);
                //echo "<table><tr><td>".$profesor->getId()."</td><td>".$profesor->getNombre()."</td></tr></table>";
                $modelo->createProfesor($profesor);
                header("Location:../vista/VistaProfesores.php") ;
            }

            if(recoge('accion') =='delprof'){

                $id = recoge('id');

                $profesor = new Profesor($id, "");
                //echo "<table><tr><td>".$profesor->getId()."</td><td>".$profesor->getNombre()."</td></tr></table>";
                $modelo->deleteProfesor($profesor);

                header("Location:../vista/VistaProfesores.php") ;
            }
            
            if(recoge('accion')=="upprof"){

                $id= recoge('id');
                $nombre= recoge('nombre');

                $profesor = new Profesor($id,$nombre);

                $modelo->updateProfesor($profesor);

                header('Location:../vista/VistaProfesores.php');
            }
            
           
            //ASIGNATURA 

             if(isset($_GET['crearasig'])){
               
                $id = recoge('id');
                $nombre = recoge('nom');
                $horas = recoge('hora');
                $Idprofesor = recoge ('prof');

                $profesor1 = new Profesor ($Idprofesor, "");
                $profesor2 = $modelo->getProfesor($profesor1);        
                $asignatura = new Asignatura($id, $nombre, $horas, $profesor2);
                if($Idprofesor != "defecto"){
                    $modelo->createAsignatura($asignatura);

                }
                echo "<table><tr><td>Seleccione un profesor del listado</td></tr></table>";

                header("Location:../vista/VistaAsignaturas.php");

            }

            if(recoge('accion') == 'delasig'){

                $id = recoge('id');

                $asignatura = new Asignatura($id, "", "","");
                //echo "<table><tr><td>".$asignatura->getId()."</td><td>".$asignatura->getNombre()."</td></tr></table>";
                $modelo->deleteAsignatura($asignatura);

                header("Location:../vista/VistaAsignaturas.php") ;
            }
            
            if(recoge('accion')=='upasig'){
                $id = recoge('id');
                $nombre = recoge('nombre');
                $horas = recoge('horas');
                $Idprofesor = recoge ('idp');

                $profesor1 = new Profesor ($Idprofesor, "");
                $profesor2 = $modelo->getProfesor($profesor1);        
                $asignatura = new Asignatura($id, $nombre, $horas, $profesor2->getId());
                
                $modelo->udpdateAsignatura($asignatura);                
                header("Location:../vista/VistaAsignaturas.php") ;
            }
            
            if(isset($_POST["registro"])){
                $id = recoge('id');
                $nombre = strtolower(recoge('usuario'));
                $pass= recoge('password');
                
                $usuario= new Usuario($id,$nombre,$pass);
                
                //echo $usuario->getId()."<br>".$usuario->getUsuario()."<br>".$usuario->getPass();
                $comprobar = $modelo->ComprobarUsuario($usuario);
                if($comprobar == null){
                    $modelo->createUsuario($usuario);
                    //var_dump($comprobar);
                    header("Location:../vista/VistaLogin.php");
                }
                    header("Location:../vista/VistaRegistro.php?resp=El usuario ya ha sido registrado");
            }
            
            if(isset($_POST["login"])){
                
                $nombre = strtolower(recoge('usuario'));
                $pass= recoge('password');
                
                $usuario= new Usuario("",$nombre,$pass);
                
                //echo $usuario->getId()."<br>".$usuario->getUsuario()."<br>".$usuario->getPass();
                $comprobar = $modelo->ComprobarUsuario($usuario);
                
                if($comprobar == null){
                    header("Location:../vista/VistaLogin.php");
                   
                }else{
                    if(!isset($_SESSION)) { session_start();  }
                    $_SESSION["usuario"]=$nombre;
                    header("Location:../vista/VistaMenu.php");
                    
                }
                             
            }
            
            
?>


