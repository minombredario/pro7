
        <?php 
            include ("header.php");
            require ("../controlador/funciones.php");
            require ("../vista/Config.php");
                       
            $modelo = getModelo();
            
            conectar();
        ?>
        
        
         <h1>Gestión del Profesorado</h1>
                    
             <table width="50%" border="0" align="center">
                <tr >
                    <td class="primera_fila">Id</td>
                    <td class="primera_fila">Nombre</td>
                    <td class="sin">&nbsp;</td>
                    <td class="sin">&nbsp;</td>
                    <td class="sin">&nbsp;</td>
                </tr> 
              
                    <?php foreach($modelo->readProfesor() as $profesor): ?>
                        <tr>
                            <td><?php echo $profesor->getId()?></td>
                            <td><?php echo $profesor->getNombre()?></td>

                            <td class="bot"><a href="../controlador/accion.php?accion=delprof&id=<?php echo $profesor->getId()?>"><input type='button' name='delprof' id='del' value='Borrar'></a></td>

                            <td class='bot'><a href="editarProfesor.php?id=<?php echo $profesor->getId()?>&nombre=<?php echo $profesor->getNombre()?>"><input type='button' name='up' id='up' value='Actualizar'></a></td>
                        </tr>
                    <?php endforeach;?>
                 
                 <form action="../controlador/accion.php" method="GET">      
                    <tr>
                        <td><input type='hidden' name='id' size='10' class='centrado' value="<?php echo $modelo->getId("profesores")?>" readonly="readonly" style="fon"></td>
                        <td><input type='text' name='nombre' size='20' class='centrado' required></td>
                        <td class='bot'><input type='submit' name='crearprof' id='cr' value='Insertar'></td>
                    </tr>
                 </form>
            </table>
       
            <div style="position: relative; margin-left: 50%">
               
                    <a href='../vista/VistaMenu.php'>Volver</a>
               
                                     
            </div>
        
        <p>&nbsp;</p>
        
 <?php include ("footer.php"); ?>

