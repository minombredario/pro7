<?php 
    require ("Config.php");
    include ("../vista/header.php"); 
    require ("../controlador/funciones.php");
    
    $vista= getVista();
    
    conectar();

?>

<div class="centrado">
    
    
</div>
<table width="50%" border="0" align="center">
               
                <thead>
                    <tr>
                        <th>
                             <h2><?php echo Config::$tema[$vista] ?></h2>
                        </th>
                    </tr>
                       
                </thead>
                             
                <tr>
                    <td class="primera_fila inicio">
                        Elegir:
                    </td>
                </tr> 
            
                <tr>
                    <td style="text-align: align;">
                        <input class="boton" type="button" onclick="location.href = '../vista/VistaProfesores.php'" name= "profesor" value="Gestión Profesor"/>
                        <input class="boton" type="button" onclick="location.href = '../vista/VistaAsignaturas.php'" name="asignatura" value="Gestión Asignatura"/>
                        <input class="boton" type="button" onclick="location.href = '../vista/VistaInstalacion.php'" name="instalar" value="Instalar BBDD"/>
                        
                    </td>
                </tr>
            
                <tr >
                    <td class="primera_fila inicio">
                        Documentación por tema:
                    </td>
                </tr> 
                
                <tr>
                    <td class="resto_filas">
                        <?php 
                            foreach(Config::$Enunciado as $doc){ 
                                echo "<li><a href='../documentacion/".$doc.".pdf'>". $doc ." Enunciado</a></li>";
                            }
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="primera_fila inicio">
                        Documentación de este proyecto:
                    </td>
                </tr>
                
                <tr>
                    <td class="resto_filas">
                        <a href="../documentacion/<?php echo Config::$Documentacion[$vista];?>_documentacion.pdf">Documentación</a>
                    </td>
                </tr>
          </table>
            <div style="position: relative; margin: auto; padding: 5px; width: 150px;">
                <input type="button" value="Gestor de archivos" onclick="location.href='../index.php'">
            </div>
 <?php include ("../vista/footer.php");?> 
