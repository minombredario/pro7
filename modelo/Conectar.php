<?php

    //require_once("../vista/Config.php");
    
    class Conectar{
        
        public static function conexion(){
            
           try{
                /*$conexion= new PDO('mysql:host=localhost;dbname=ceedcv', 'alumno', 'alumno');*/

                $conexion = new PDO('mysql:host='.Config::$bdhostname.';dbname='.Config::$bdnombre , "'" . Config::$bdusuario . "'",Config::$bdclave);

                $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $conexion->exec("SET CHARACTER SET utf8");

                return $conexion;

            } catch (Exception $e) {

                die ('Error: ' . $e->getMessage());

                echo "la línea de error es: " . $e->getLine();//devuelve la linea donde esta el error
            }
        }
        
       public static function ConexionSinBD($dbname, $dbuser, $dbpass, $dbhost){
            try{
                /*$base= new PDO('mysql:host=localhost;dbname=pruebas', 'root', '');*/
                
                $conexion = new PDO('mysql:host=' . $dbhost . ';dbname='. $dbname, $dbuser, $dbpass);

                $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $conexion->exec("SET CHARACTER SET utf8");

                return $conexion;

            } catch (Exception $e) {

                die ('Error: ' . $e->getMessage());

                echo "la línea de error es: " . $e->getLine();//devuelve la linea donde esta el error
            }
        }
    }
    
  
?>

 