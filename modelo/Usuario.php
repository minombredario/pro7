<?php

    class Usuario{
        
        private $id;
        private $usuario;
        private $pass;
        
        
        public function __construct($id, $usuario, $pass) {
            $this->id = $id;
            $this->usuario = $usuario;
            $this->pass = $pass;
        }
        
        function getId() {
            return $this->id;
        }

        function getUsuario() {
            return $this->usuario;
        }

        function getPass() {
            return $this->pass;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setUsuario($usuario) {
            $this->usuario = $usuario;
        }

        function setPass($pass) {
            $this->pass = $pass;
        }


    }

?>

