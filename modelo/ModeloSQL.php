<?php
require_once("../database/configBD.php");
//require ("iModelo.php");

class ModeloSQL implements iModelo{
    
    protected $conexionDB ;
    protected $conexion;
    private $profesores;
    private $asignaturas;
    
                    //      BASE DE DATOS       //
    public function __construct(){
        
        require ("../database/Conectar.php");
               
        $this->conexionDB = NULL;
        
        $this->conexionDB = Conectar::conexion();
        
        $this->profesores = array();
        $this->asignaturas = array();
    }
    
    
     //          USUARIOS        //
    
    public function createUsuario($usuario){
        
        $nombre = $usuario->getUsuario();
        $pass = $usuario->getPass();
        
        $sql = "INSERT INTO usuarios (usuario,password) VALUES(:user,:password);";
    
        $consulta=$this->conexionDB->prepare($sql);

        $consulta->execute(array(":user"=>$nombre,":password"=>$pass));
            
          
    }
    
    
      
    public function readUsuario(){
        $consulta=$this->conexionDB->query("SELECT * FROM usuarios ORDER BY id ASC");
        $usuarios = array();
        
        while($registros=$consulta->fetch(PDO::FETCH_ASSOC)){
                $usuario = new Usuario($registros["id"],$registros["usuario"],$registros["password"]);
                array_push($usuarios, $usuario);
        }          
            
        return $usuarios;
    }
        
    public function ComprobarUsuario($usuario){
        $nombre = $usuario->getUsuario();
        $pass = $usuario->getPass();
        
        $usuario = new Usuario("",$nombre, $pass);
       try{
            $sql = ("SELECT * FROM usuarios WHERE usuario= :user AND password = :password;");
    
            $resultado = $this->conexionDB->prepare($sql);
    
            $resultado->bindValue(":user", $nombre);
            $resultado->bindValue(":password",$pass);
    
            $resultado->execute();
        
            if($resultado->rowCount() != 0){
               return $usuario;
            }else{
                return null;
            }
       } catch (Exception $ex) {

       } 
       
    }
    
    
                        //      PROFESORES     //
    public function createProfesor($profesor) {
        $nombre = $profesor->getNombre();
        //creo una sentencia preparada para evitar inyeccion de codigo sql
        $sql="INSERT INTO profesores (Nombre) VALUES (:nombre)";

        $consulta=$this->conexionDB->prepare($sql);

        $consulta->execute(array(":nombre"=>$nombre));
    }
    
    
    public function readProfesor() {
        $consulta=$this->conexionDB->query("SELECT * FROM profesores ORDER BY id ASC");
        $profesores = array();
             
        
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
                $profesor = new Profesor($registro["id"],$registro["nombre"]);
                array_push($profesores, $profesor);
        }          
            
        return $profesores;
    }
    

    
    public function updateProfesor($profesor) {
        $id = $profesor->getId();
        $nombre = $profesor->getNombre();
               
        //creo una sentencia preparada para evitar inyeccion de codigo sql
        $sql="UPDATE profesores SET nombre=:miNom WHERE id=:miId";
        
        $consulta=$this->conexionDB->prepare($sql);
        
        $consulta->execute(array(":miId"=>$id, ":miNom"=>$nombre));
        
    }
    
    
    public function deleteProfesor($data) {
             
        try{
            $id=$data->getId();
        
            $sql=('DELETE FROM profesores WHERE id='.$id);
            $consulta=$this->conexionDB->prepare($sql);
            $consulta->execute(array());
        } catch (Exception $ex) {
            echo "<table><tr><td>No se puede eliminar el profesor</td></tr><tr><td>Compruebe asignaturas asignadas a él</td></tr></table>";
        }
       
        
    }
    
    
    
    public function getProfesor($data){
        $id = $data->getId();
        $profesor = "";
        $consulta=$this->conexionDB->query("SELECT * FROM profesores where id=".$id.";");
        
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
               if($registro["id"]==$id){
                   $profesor = new Profesor($registro["id"],$registro["nombre"]);
               }
               
        }
        return $profesor;
    }
    
    
                    //      CALCULAR ID     //
    
     public function getId($tabla){
        $id = $this->conexionDB->query("SELECT MAX(id) AS id FROM ".$tabla.";")->fetch(PDO::FETCH_NUM);
        return $id[0] + 1;
    }   
    
                    //      ASIGNATURAS     //
    
    public function createAsignatura($asignatura) {
        
       $nombre = $asignatura->getNombre();
       $hora =  $asignatura->getHoras();
       $prof =$asignatura->getProfesor()->getId();
       
        //creo una sentencia preparada para evitar inyeccion de codigo sql
        $sql="INSERT INTO asignaturas (nombre, horas, id_profesor) VALUES (:nombre, :horas, :profesor)";

        $consulta=$this->conexionDB->prepare($sql);

        $consulta->execute(array(":nombre"=>$nombre, ":horas"=>$hora, ":profesor"=>$prof));
   }
   
   
   public function readAsignatura() {
        $consulta=$this->conexionDB->query("SELECT asignaturas.id, asignaturas.nombre, asignaturas.horas, asignaturas.id_profesor as idp, profesores.nombre as profesor FROM profesores,asignaturas where profesores.id = asignaturas.id_profesor ORDER BY id ASC");
        $asignaturas = array();
             
        
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
                $profesor = new Profesor($registro["idp"], $registro["profesor"]);
                $asignatura = new Asignatura($registro["id"],$registro["nombre"],$registro["horas"],$profesor);
                array_push($asignaturas, $asignatura);
        }          
            
        return $asignaturas; 
    }
    
    public function udpdateAsignatura($asignatura) {
        $id = $asignatura->getId();
        $nombre = $asignatura->getNombre();
        $horas = $asignatura->getHoras();
        $profesor = $asignatura->getProfesor();
        
        try{
        //creo una sentencia preparada para evitar inyeccion de codigo sql
            $sql="UPDATE asignaturas SET nombre=:miNom, horas=:miHora, id_profesor=:miProfesor WHERE id=:miId;";
        
            $consulta=$this->conexionDB->prepare($sql);
        
            $consulta->execute(array(":miId"=>$id, ":miNom"=>$nombre, ":miHora"=>$horas, ":miProfesor"=>$profesor));
        } catch (Exception $ex) {

        }
        
    }
    
    

    public function deleteAsignatura($data) {
        
        $id=$data->getId();
        
        $sql=('DELETE FROM asignaturas WHERE id='.$id);
        $consulta=$this->conexionDB->prepare($sql);
        $consulta->execute(array());
    }

    

    public function desinstalar() {
        
    }

       
    public function instalarBD() {
        require ("../database/ConectarServidor.php");
        $this->conexion = ConectarServidor::conectar();
        
        echo "<h2>Instalando: " . Config::$modelo . "</h2>";
               
   
     try {
            $crearDB = $this->conexion->prepare('CREATE DATABASE IF NOT EXISTS ' . BASE . ' COLLATE utf8_spanish_ci');
            $crearDB->execute();
            echo "Creada BDDD: " . BASE . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }
     
     }
     function CrearTablas(){
               
        try {
            $use_db = $this->conexion->prepare('USE ' . BASE);
            $use_db->execute();
            //creamos la tabla profesor
            $sql = ("CREATE TABLE IF NOT EXISTS profesores "
                    . "(id INT AUTO_INCREMENT PRIMARY KEY,"
                    . " nombre VARCHAR(20) COLLATE utf8_spanish_ci)");

            //echo $sql . "<br>";
            
            $crear_tb_profesor = $this->conexion->prepare($sql);
            $crear_tb_profesor->execute();
            echo "Creada tabla: profesores<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = ("CREATE TABLE IF NOT EXISTS asignaturas"
                . "( id INT AUTO_INCREMENT PRIMARY KEY,"
                . " nombre VARCHAR(90) COLLATE utf8_spanish_ci NOT NULL,"
                . " horas INT (4) NOT NULL,"
                . " id_profesor INT NOT NULL);");
            
            
            //echo $sql . "<br>";
            $crear_tb_asignatura = $this->conexion->prepare($sql);
            $crear_tb_asignatura->execute();
            echo "Creada tabla: asignaturas<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }
     }
     function CrearClaves(){
        
        try {
            $use_db = $this->conexion->prepare('USE ' . BASE);
            $use_db->execute();
            $sql = ("ALTER TABLE asignaturas ADD FOREIGN KEY (id_profesor) REFERENCES profesores (id);");
            
            $fk = $this->conexion->prepare($sql);
            $fk->execute();
            echo "Creada CAj: Asignaturas.id_profesor-> profesores.id<br>";
            
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        $conexion = false;
    }

    public function rellenarTablas(){
        
        $this->conexionDB->query("INSERT INTO profesores (nombre) VALUES ('Paco');"
                . "INSERT INTO profesores (nombre) VALUES ('Alfredo');"
                . "INSERT INTO profesores (nombre) VALUES ('Sergio');"
                . "INSERT INTO profesores (nombre) VALUES ('Carlos');");
        
            
        $this->conexionDB->query("INSERT INTO asignaturas (nombre, horas, id_profesor) VALUES ('Desarrolo aplicaciones Web entorno Servidor', 300, 1);"
            . "INSERT INTO asignaturas (nombre, horas, id_profesor) VALUES ('Diseño de Iterfaces Web', 500, 2);"
            . "INSERT INTO asignaturas (nombre, horas, id_profesor) VALUES ('Desarrollo de apilicaciones Web entorno Cliente', 250, 3);"
            . "INSERT INTO asignaturas (nombre, horas, id_profesor) VALUES ('Despligue de aplicaciones Web', 800, 4);");
       
    }

    

}
   

?>