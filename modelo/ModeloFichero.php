<?php

require ("Profesor.php");
require ("Asignatura.php");
require ("Usuario.php");
require ("iModelo.php");
//require_once ("../controlador/funciones.php");

    
class ModeloFichero implements iModelo{
    
    private $fasig = ("../ficheros/asignaturas.csv");
    private $fprof= ("../ficheros/profesores.csv");
    private $fuser = ("../ficheros/usuarios.csv");
    
    
                //          USUARIOS        //
    
    public function createUsuario($usuario){
             
        $file = fopen($this->fuser,"a");
        $linea =  $usuario->getId() . ";"
                . $usuario->getUsuario() . ";"
                . $usuario->getPass(). "\r\n";
        fputs($file,$linea);
        fclose($file);
       
    }
    
    
      
    public function readUsuario(){
        $usuarios = array();
        if(!file_exists($this->fuser)){
           $usuario = new Usuario("","","");
            return   $usuario;
        }else if (filesize($this->fuser)==0){
            unlink($this->fuser);
            $usuario = new Usuario("","","");
            return   $usuario;
        }else if($file = fopen($this->fuser,"r")){
            $registros = fgetcsv($file, 0, ";");
        
            while($registros){
                $usuario = new Usuario($registros[0],$registros[1],$registros[2]);
                array_push($usuarios,$usuario);
                $registros = fgetcsv($file, 0, ";");
            }
            
            fclose($file);
        return $usuarios;  
        }
    }
        
    public function ComprobarUsuario($usuario){
       
        $nombre = $usuario->getUsuario();
        $pass = $usuario->getPass();
        
        if(!file_exists($this->fuser)){
            return null;
        }else if( $file = fopen($this->fuser,"r")){
           
            $registros = fgetcsv($file, 0, ";");
        
            while($registros){
                if($nombre == $registros[1] && $pass == $registros[2]){
                    $usuario = new Usuario($registros[0],$registros[1],$registros[2]);
                    return $usuario;

                }else{
                    break;
                }
            } 
        }
    }
    
        
    
                 //          PROFESORES      //
    
    public function createProfesor($profesor){
       
        $file = fopen($this->fprof, "a");
        $linea = $profesor-> getId(). ";"
                . $profesor->getNombre(). "\r\n";
        fwrite($file, $linea);
        fclose($file);
        
    }
    
    public function readProfesor(){
        
        $profesores = array();
        if(!file_exists($this->fprof)){
           $profesor = new Profesor("","");
            return   $profesor;
        }else if (filesize($this->fprof)==0){
            unlink($this->fprof);
            $profesor = new Profesor("","");
            return   $profesor;
        }else if($file = fopen($this->fprof,"r")){
                $registros = fgetcsv($file, 0, ";");
                    while($registros){
                        $profesor = new Profesor($registros[0],$registros[1]);
                        array_push($profesores, $profesor);
                        $registros = fgetcsv($file, 0, ";");
                        }
                    fclose($file);
                           
        return $profesores;}
    }
        
    public function getProfesor($data){
        $file = fopen($this->fprof, "r");
        $registros = fgetcsv($file, 1000, ";");
        $profesor = new Profesor("", "");
        
        while ($registros) {
            $idp = $registros[0];
            
            if ($idp == $data->getId()) {
                $profesor->setId($registros[0]);
                $profesor->setNombre($registros[1]);
                break;
            }

            $registros = fgetcsv($file, 1000, ";");
        }
        fclose($file);
        
        return $profesor;
    }
    
    
    public function updateProfesor($profesor) {
        
        $id = $profesor->getId();
        
        if ($file = fopen($this->fprof, "r+")) {
            $temp = fopen("../ficheros/prof_temp.csv", "w+");
            $registros = fgetcsv($file, 0, ";");
            
            while ($registros) {
                if ($id != $registros[0]) {
                    fputcsv($temp, $registros, ";");
                } else {
                    $linea = array($profesor->getId(), $profesor->getNombre());
                    fputcsv($temp, $linea, ";");
                }
                $registros = fgetcsv($file, 0, ";");
            }
            
            fclose($file);
            fclose($temp);
        }
        unlink("../ficheros/profesores.csv");
        rename("../ficheros/prof_temp.csv", "../ficheros/profesores.csv");
    }

    
    
    
    public function deleteProfesor($data) {
        
        //echo "<table><tr><td>".$profesor->getId()."</td><td>".$profesor->getNombre()."</td></tr></table>";
        $profesor = $this->getProfesor($data);
        $profesores = array();
        $profesores = $this->readProfesor();//guardo el listado de todos los profesores en un array
        $posicion = array_search($profesor,$profesores);//guardo la posicion del objeto 
        //echo $posicion;      
        unset($profesores[$posicion]);//elimino la poscion del objeto
        $profesores = array_values($profesores);//indexo de nuevo el array
        
        $file = fopen($this->fprof, "w");
            foreach($profesores as $profesor){
                $this->createProfesor($profesor);
            }
    }
    
    
    
                    //      CALCULAR ID     //
    
    function getId($fichero) {
    
        $ultId="0";
        $registros= array();

        if(is_file("../ficheros/".$fichero.".csv")){

            if($fichero=="profesores"){
                $registros = $this->readProfesor();//guardo todos los registros del .csv en un array
            }
            if($fichero=="asignaturas"){

                $registros = $this->readAsignatura();//guardo todos los registros del .csv en un array
            }
            
            if($fichero == "usuarios"){
                $registros = $this->readUsuario();
            }
            $ultregistro = end($registros);//con end() averiguo cual es el ultimo elemento del array
            $ultId = $ultregistro->getId();
            $ultId++;
            return $ultId; 
        }else{
            $ultId = "1";
            return $ultId; 

        }
    }
    
    
                //      ASIGNATURAS     //
    
    public function createAsignatura($asignatura){
        $file = fopen($this->fasig, "a");
        $linea = $asignatura->getId(). ";"
                . $asignatura->getNombre(). ";"
                . $asignatura->getHoras().";"
                . $asignatura->getProfesor()->getId()
                ."\r\n";
        fwrite($file, $linea);
        fclose($file);
    }
    
    
    
    
    public function createAsignatura_($asignatura){
        $file = fopen($this->fasig, "a");
        $linea = $asignatura->getId(). ";"
                . $asignatura->getNombre(). ";"
                . $asignatura->getHoras().";"
                . $asignatura->getProfesor()
                ."\r\n";
        fwrite($file, $linea);
        fclose($file);
    }
    
    
        
    public function readAsignatura(){
        $asignaturas = array();
      
        if(!file_exists($this->fasig)){ 
          $asignatura = new Asignatura("","","","");
          return   $asignatura;
          
        }else if (filesize($this->fasig)==0){
            unlink($this->fasig);
            $asignatura = new Asignatura("","","","");
            return   $asignatura;
            
        }else if($file = fopen($this->fasig,"r")){
                $registros = fgetcsv($file, 0, ";");
                    while($registros){
                        $profesor = new Profesor($registros[3],"");
                        $profesor2 = $this->getProfesor($profesor);
                        $asignatura = new Asignatura($registros[0],$registros[1],$registros[2],$profesor2);
                        array_push($asignaturas, $asignatura);
                        $registros = fgetcsv($file, 0, ";");

                    }
                    fclose($file);
            }
            return $asignaturas;
        }        
            
    public function udpdateAsignatura($asignatura) {
        $id = $asignatura->getId();
        
        if ($file = fopen($this->fasig, "r+")) {
            $temp = fopen("../ficheros/asig_temp.csv", "w+");
            $registros = fgetcsv($file, 0, ";");
            
            while ($registros) {
                if ($id != $registros[0]) {
                    fputcsv($temp, $registros, ";");
                } else {
                    $linea = array($asignatura->getId(), $asignatura->getNombre(),$asignatura->getHoras(),$asignatura->getProfesor());
                    fputcsv($temp, $linea, ";");
                }
                $registros = fgetcsv($file, 0, ";");
            }
            
            fclose($file);
            fclose($temp);
        }
        unlink("../ficheros/asignaturas.csv");
        rename("../ficheros/asig_temp.csv", "../ficheros/asignaturas.csv");
    }  
    
    
    public function deleteAsignatura($data) {
        $asignatura = $this->getAsignatura($data);
        $asignaturas = array();
        $asignaturas = $this->readAsignatura();
        $posicion = array_search($asignatura, $asignaturas);
        unset($asignaturas[$posicion]);
        $asignaturas = array_values($asignaturas);       
        
        $file = fopen($this->fasig, "w");
            foreach($asignaturas as $asignatura){
                $this->createAsignatura($asignatura);
            }
        
    }
        
    
        
    public function getAsignatura($data){
        $file = fopen($this->fasig, "r");
        $registros = fgetcsv($file, 1000, ";");
        $asignatura = new Asignatura("", "", "", "");
        
        while ($registros) {
            $ida = $registros[0];
            
            if ($ida == $data->getId()) {
                $asignatura->setId($registros[0]);
                $asignatura->setNombre($registros[1]);
                $asignatura->setHoras($registros[2]);
                $asignatura->setProfesor($registros[3]);
                break;
            }

            $registros = fgetcsv($file, 1000, ";");
        }
        fclose($file);
        
        return $asignatura;
    }    
        
    
    
    
    public function instalarBD(){
        echo "BBDD creada";
            
                 
    }
    
    public function rellenarTablas(){
            $this->createProfesor(new Profesor(1,"Paco"));
            $this->createProfesor(new Profesor(2,"Alfredo"));
            $this->createProfesor(new Profesor(3,"Sergio"));
            $this->createProfesor(new Profesor(4,"Carlos"));
        
            $this->createAsignatura_(new Asignatura(1,"Desarrolo aplicaciones Web entorno Servidor",300,1));
            $this->createAsignatura_(new Asignatura(2,"Diseño de Iterfaces Web",500,2));
            $this->createAsignatura_(new Asignatura(3,"Desarrollo de apilicaciones Web entorno Cliente",250,3));
            $this->createAsignatura_(new Asignatura(4,"Despligue de aplicaciones Web",800,4));
            echo "Tablas creadas";
    }
    
    public function crearTablas(){
        if (!file_exists($this->fprof)) {
	    $archivo = fopen($this->fprof, "a");
            
            fclose($archivo);
            echo "Creada tabla: profesores<br>";
            
        }else{
            echo "Ya existe tabla: profesores<br>";
        }
        
        if (!file_exists($this->fasig)) {
	    $archivo = fopen($this->fasig, "a");
            
            fclose($archivo);
            echo "Creada tabla: asignaturas<br>";
        }else{
            echo "Ya existe tabla: asignaturas<br>";
        }
            
    }
    
    function CrearClaves(){
        echo "Creada CAj: Asignatura.idp -> Profesor.id <br>";
    }
    
    public function desinstalar(){
           
    }

    

}


?>
